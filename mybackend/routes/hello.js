const express = require('express')
const router = express.Router()

router.get('/', (req, res, next) => {
  res.json({ message: 'Hello ya' })
})

router.get('/:id', (req, res, next) => {
  const { params } = req
  res.json({ message: 'Hello', params })
})

module.exports = router
